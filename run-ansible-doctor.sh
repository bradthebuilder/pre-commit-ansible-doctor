#!/usr/bin/env bash
printenv | grep PATH
DOCTOR_DIR=/home/$USER/.local/bin
if [[ ${DOCTOR_DIR} != *${PATH}* ]]; then
    export PATH=$PATH:$DOCTOR_DIR
fi
ansible-doctor -f
